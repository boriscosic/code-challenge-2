## Simple CMS Developer Challenge
At Trufla we love open source and our client sites are powered mainly by SilverStripe or Wordpress.
For this challenge there is no preference given to CMS, pick either to complete the goal. 
This task is about leveraging CSS-based design, points are given to mobile first, semantically correct code.

### Goal
Often we take design and need to convert to something beautiful. For this task the design and code exist.
Your challenge is to take the static [HTML](http://www.csszengarden.com/examples/index) 
and [CSS](http://www.csszengarden.com/214/214.css) files and create a theme in the CMS of your choice.
Content of the HTML should be managable inside CMS. Contents of `<nav role="navigation">` should be generated 
from the CMS. Show us your approach for crafting a theme, your build tools and attention to responsive design.

### Bonus

- Convert CSS to SCSS.
- Create modular component on the CMS page.
- Add ability to deploy in Docker container.
- Add GitLab compatible CI/CD to perform a server deploy.